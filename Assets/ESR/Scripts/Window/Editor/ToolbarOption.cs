﻿using UnityEngine;
using UnityEditor; //MenuItem needs that. Probably you want to put that class inside your "Editor" folder
namespace YJack.ESR
{
/// <summary>
/// This class adds a MenuItem in our Menubar.
/// </summary>
public class ToolbarOption : MonoBehaviour
{
    [MenuItem("Something/Whatever/DO #%w")]
    static void DoSomething()//Need to be static and UnityEditor is needed
    {
        Debug.Log("Something");
    }
}
}