﻿using UnityEngine;
namespace YJack.ESR
{
/// <summary>
///  A Gizmos is a sign, a non-interactive representative structure of something else. Usually Gizmos are rendered above everything and there's no particular function for a Gizmo than represent something (Useful on Debug).
/// </summary>
public class DrawGizmos : MonoBehaviour
{
	void OnDrawGizmosSelected()//This method draw a Gizmo Into our Viewport when the object is selected.
    {
        Gizmos.DrawIcon(transform.position, "YICON"); //DrawIcon treats OnDrawGizmosSelected as OnDrawGizmos method. Icon image should be placed inside a folder called Gismos
        Gizmos.color = Color.yellow;
		Gizmos.DrawSphere(transform.position, 1);
	}
}
}