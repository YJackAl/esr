﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace YJack.ESR
{
/// <summary>
/// A simple class with float value. We did a Handle to change this variable.
/// </summary>
public class DrawHandles : MonoBehaviour
{
	public float areaOfEffect = 5;
}

/// <summary>
/// The class below draws a Handle. Handles are interactive so more than just a representation, handles are tools to visually manipulate variables.
/// </summary>
#if UNITY_EDITOR
[CustomEditor (typeof(DrawHandles))]
class DrawSolidDisc:Editor
{
	DrawHandles drawHandles
	{
		get
		{
			return ((DrawHandles)target);
		}
	}

	void OnSceneGUI ()
	{
		Handles.color = new Color(1f,0f,0f,0.1f);
		Handles.DrawSolidDisc(drawHandles.transform.position, Vector3.up, drawHandles.areaOfEffect);
		Handles.color = Color.red;
		drawHandles.areaOfEffect = Handles.ScaleValueHandle(drawHandles.areaOfEffect,drawHandles.transform.position + new Vector3(drawHandles.areaOfEffect,0,0),Quaternion.identity,2,Handles.CylinderHandleCap,2);


        // start of 2D UI section
        Handles.BeginGUI();
        Handles.Label(drawHandles.transform.position + Vector3.right * 2, "Area Of Effect: " + drawHandles.areaOfEffect);
        GUILayout.BeginArea(new Rect(Screen.width - 100, Screen.height - 80, 90, 50));
        if (GUILayout.Button("Reset Area")) { drawHandles.areaOfEffect = 5; }
            GUILayout.EndArea();
        Handles.EndGUI();
    }
}
#endif
}