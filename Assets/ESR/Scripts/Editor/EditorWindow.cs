﻿using UnityEngine;
using UnityEditor;

namespace YJack.ESR
{
    public class MyWindow : EditorWindow
    {
        [MenuItem("Window/My Window")]
        public static void ShowWindow()
        {
            EditorWindow.GetWindow(typeof(MyWindow)); //Or using GetWindowWithRect
        }

        void OnGUI()
        {
            GUILayout.Label("Some new Window", EditorStyles.boldLabel);
            EditorGUILayout.LabelField("Use GUI, GUILayout, EditorGUI, EditorGUILayout and Others Editor Classes to put something cool here");
            if (GUILayout.Button("Do something amazing"))
                Debug.Log("Amazing Log");
        }
    }
}