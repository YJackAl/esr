﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace YJack.ESR
{
public class DefaultInspector : MonoBehaviour
{
    public CustomizedClass mySomeClass;
}

#if UNITY_EDITOR
[CustomEditor(typeof(DefaultInspector))]
public class DefaultInspectorDrawer : Editor
{
    public override void OnInspectorGUI()
    {
        GUILayout.Label("This is above DrawDefaultInspector();");
        DrawDefaultInspector(); //Draws the Default Inspector
        GUILayout.Label("This is below DrawDefaultInspector();");
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        GUILayout.Label("I used EditorGUILayout.PropertyField to serialize the class below");
        SerializedProperty serializedProperty = serializedObject.FindProperty("mySomeClass"); //Serialize this property on the standard way of serialize this kind of variable.
        EditorGUILayout.PropertyField(serializedProperty, true);
    }
}
#endif
}