﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace YJack.ESR
{
    /// <summary>
    /// This example is very similar to "CustomClassWithDrawer" but here we defined a diferent Drawer for float and create a Custom Drawer that we can easily refer from our MonoBehaviour.
    /// </summary>
    public class CustomPropertyAtribute : MonoBehaviour
    {
        [MyProgressBar()] //This is our Custom Property Drawer changing how the float below is supposed to be draw.
        public float progressFloat;
        [MyProgressBar("My Progress Bar")]//This is our Custom Property Drawer, passing one parameter, changing how the float below is supposed to be draw.
        public float secondProgressFloat;
    }

    /// <summary>
    /// This class is actually a PropertyAttribute. A PropertyAttribute can be refered into our MonoBehaviour as the native PropertyAttribute.
    /// </summary>
    public class MyProgressBar : PropertyAttribute
    {
        public string text; //We create an optional parameter to illustrate the possibility of overload constructor method here.

        public MyProgressBar(string text)
        {
            this.text = text;
        }

        public MyProgressBar()
        { }
    }


    /// <summary>
    /// This is a regular PropertyDrawer similar with the one used on CustomClassWithDrawer but here it describes how to draw a PropertyAttribute instead of a MonoBehaviour.
    /// </summary>
#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(MyProgressBar))]
    public class MYProgressBarDrawer : PropertyDrawer
    {
        MyProgressBar progressBar //Just a property. It convert attributo to our desired type "ProgressBar"
        {
            get
            {
                return ((MyProgressBar)attribute);
            }
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            Rect myRect = new Rect(position.x, position.y, position.width, position.height);
            float myValue = property.floatValue;
            string myText = progressBar.text;

            if (myText == null)
                myText = "Progress";
            EditorGUI.ProgressBar(myRect, myValue, myText);
        }
    }
#endif
}