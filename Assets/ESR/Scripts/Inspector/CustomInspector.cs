﻿using UnityEngine;
#if UNITY_EDITOR//We just need this namespace at editor-time. In a real project you may want to put everything that is inside this condition inside a separate file inside "Editor" folder along the apropriate class
using UnityEditor;
#endif
namespace YJack.ESR
{
    /// <summary>
    /// A dummy class so we can focus on the editor-side and see how a Custom Inspector is done.
    /// </summary>
    public class CustomInspector : MonoBehaviour
    {
    }

#if UNITY_EDITOR//We just need this namespace at editor-time. In a real project you may want to put everything that is inside this condition inside a separate file inside "Editor" folder along the "UnityEditor" namespace
    [CustomEditor(typeof(CustomInspector))]//Here we say: The class below is the editor-side from a class called "MyInspectorGUI"
    public class TestOnInspector : Editor//We inherit from Editor
    {
        public override void OnInspectorGUI()//This method overrides the one that is used to draw the Inspector
        {
            GUILayout.Label("OnInspectorGUI example");// A simple label done using GUILayout class. This is just an example inside the editor class to be exibited into Inspector. You can consider that a "Hello World"
            GUIContent guiContentSample = new GUIContent("GUIContent Sample", "That's a fast way to add an Tooltip when doing your Editor Code. Can also add an image!");
            GUILayout.Label(guiContentSample);// Using GUIContentSample
        }
    }
#endif
}