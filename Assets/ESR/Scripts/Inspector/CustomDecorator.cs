﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace YJack.ESR
{
    /// <summary>
    /// Similar to the CustomProperty example, this also use a PropertyAttribute but here our Drawer is a DecoratorDrawer instead of a PropertyDrawer
    /// </summary>
    public class SeparatorAttribute : PropertyAttribute
    {
        public readonly string title;

        public SeparatorAttribute()
        {
        }

        public SeparatorAttribute(string _title)
        {
            this.title = _title;
        }
    }


    /// <summary>
    /// Here is our DecoratorDrawer. As you can see, on DecoratorDrawer we have no SerializedProperty to deal with. So we are restricted to decorative elements. Otherwise we should use Property Drawers.
    /// </summary>
#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(SeparatorAttribute))]
    public class separatorDrawer : DecoratorDrawer
    {
        SeparatorAttribute separatorAttribute
        {
            get
            {
                return ((SeparatorAttribute)attribute);
            }
        }


        public override void OnGUI(Rect _position)
        {
            if (separatorAttribute.title == null)
            {
                _position.height = 1;
                _position.y += 10;
                GUI.Box(_position, "");
            }
            else
            {
                Vector2 textSize = GUI.skin.label.CalcSize(new GUIContent(separatorAttribute.title));
                float separatorWidth = (_position.width - textSize.x) / 2.0f - 5.0f;
                _position.y += 10;

                GUI.Box(new Rect(_position.xMin, _position.yMin, separatorWidth, 1), "");
                GUI.Label(new Rect(_position.xMin + separatorWidth + 5.0f, _position.yMin - 8.0f, textSize.x, 20), separatorAttribute.title);
                GUI.Box(new Rect(_position.xMin + separatorWidth + 10.0f + textSize.x, _position.yMin, separatorWidth, 1), "");
            }
        }

        public override float GetHeight() //This method override the default Height used by this property.
        {
            return 21.0f;
        }
    }
#endif
}