﻿using UnityEngine;
using System.Collections.Generic; //This namespace is where List type is.
namespace YJack.ESR
{
/// <summary>
/// Arrays, List<T>, primitive types (int,float,dougle,bool,string, etc) can be serialized if public. Everything that inherits from UnityEngine.Object are also exposed at Editor Side at the same circumstances.
/// </summary>
public class BuiltIn : MonoBehaviour
{
	public bool myPrimitiveType; //Public and primitive type is serialized
	public GameObject inheritedFromObject;//Public and inherits from UnityEngine.Object is serialized
    public float[] myArray;//Public and primitive type is serialized
    public List<int> myList;//List are serialized.
}
}