﻿using System;
using UnityEngine;
#if UNITY_EDITOR//We just need this namespace at editor-time. In a real project you may want to put everything that is inside this condition inside a separate file inside "Editor" folder along the apropriate class
using UnityEditor;
#endif
namespace YJack.ESR
{
/// <summary>
/// This class was creater as an example of Drawer. As you can see our class is drawed exactly as decribed by the correspondent PropertyDrawer even inside an array.
/// </summary>
public class CustomClassWithDrawer : MonoBehaviour
{
	public CustomizedClass[] mySomeClass; //An array of our "CustomizedClass". 
}
/// <summary>
/// This is the class that we gonna implement a PropertyDrawer. It's composed by a string and an Enum. This class isn't a MonoBehaviour
/// </summary>
[Serializable]
public class CustomizedClass
{
	public string myString;
	public SomeEnum myEnum;
}

/// <summary>
/// This is a Property Drawer for "CustomizedClass". It tells Unity how this class should be draw.
/// </summary>
#if UNITY_EDITOR//We just need this namespace at editor-time. In a real project you may want to put everything that is inside this condition inside a separate file inside "Editor" folder along the "UnityEditor" namespace
[CustomPropertyDrawer(typeof(CustomizedClass))] //Here we say: The class below is the property drawer from a class called "CustomizedClass"
public class CustomizedClassEditorSide : PropertyDrawer//We inherit from PropertyDrawer
{
	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)//EdiotrGUILayout Are not usable With Property Drawers. NOTE: We did a brief comparison between the main drawer method from the main editor-side classes on the Readme.0
    {
		SerializedProperty myString = property.FindPropertyRelative("myString"); //We take a reference for the SerializableProperty string inside the SerializedProperty of "CustomizedClass"
        SerializedProperty myEnum = property.FindPropertyRelative("myEnum");//We take a reference for the SerializableProperty enum inside the SerializedProperty of "CustomizedClass"

        Rect myStringRect = new Rect (position.x,position.y,position.width/2,position.height);//Here we specify the rect to draw the the string parameter
		Rect myEnumRect = new Rect (position.x + position.width/2,position.y,position.width/2,position.height);//Here we specify the rect to draw the the enum parameter

        EditorGUI.PropertyField(myStringRect, myString);//We draw the string parameter using the default string drawer inside the specified Rect
		EditorGUI.PropertyField(myEnumRect, myEnum); //We draw the enum parameter using the default enum drawer inside the specified Rect
    }
}
#endif
}