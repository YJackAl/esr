﻿using System; //This is needed so we can use [Serializable]
using UnityEngine;
using UnityEngine.Events; //This namespace is where UnityEvent are
using System.Collections.Generic;
namespace YJack.ESR
{
/// <summary>
/// Custom classes should receive the property: System.Serializable, in order to be Serializable. Structs and Enums are also serializable. A Serializable  property can serialize other property inside it up to 7 steps. (AKA:A serialized field inside a serialized field inside a serialized field insider a serialized field inside a serialized field inside a serialized field inside a serialized field).
/// </summary>
public class NativeSerialization : MonoBehaviour
{
	public bool myBool; //primitive type
	public int myInteger;//primitive type
    public float myFloat;//primitive type
    public string myString;//primitive type
    public Vector2 myVector2;//inherits from UnityEngine.Object
    public Vector3 myVector3;//inherits from UnityEngine.Object
    public Quaternion myQuaternion;//inherits from UnityEngine.Object
    public Vector4 myVector4;//inherits from UnityEngine.Object
    public Rect myRect;//inherits from UnityEngine.Object
    public AnimationCurve myAnimationCurve;//inherits from UnityEngine.Object
    public Color myColor;//inherits from UnityEngine.Object
    public Sprite mySprite;//inherits from UnityEngine.Object
    public Texture myTexture;//inherits from UnityEngine.Object
    public Material myMaterial;//inherits from UnityEngine.Object
    public AudioClip myAudioClip;//inherits from UnityEngine.Object
    public GameObject myGameObject;//inherits from UnityEngine.Object
    public LayerMask myLayerMask;//inherits from UnityEngine.Object
    public GameObject[] myGameObjectArray;//inherits from UnityEngine.Object
    public List<GameObject> myGameObjectList;//List of a type that inherits from UnityEngine.Object
    public SomeEnum mySomeEnum;//Enum are serializable
    public SomeStruct mySomeStruct;// Structs are serializable
	public SomeClass mySomeClass; //This is a class that we create  and is serializable
	public List<SomeClass> mySomeClassList;//This is a list from class that we create  and is serializable
    public UnityEvent myUnityEvent;////inherits from UnityEngine.Object
}

/// <summary>
/// Some Enum with three options. You can see about enum used as flag in "EnumMask" script
/// </summary>
public enum SomeEnum
{
	SomeOption,
	SomeOtherOption,
	SomeOtherOptionAgain
}

/// <summary>
/// Some struct
/// </summary>
[Serializable] //Using that our struct is serialized and we can fill there field into the inspector(string and int)
public struct SomeStruct
{
	public string someString;
	public int someInt;
}

/// <summary>
/// This is a custom class. one of his field is another custom class that is also serializable. You can see both into Inspector.
/// </summary>
[Serializable]//Using that our class is serialized
public class SomeClass
{
	public GameObject myGameObject;
	public SomeOtherClass mySomeOtherClass;
}

/// <summary>
/// This classes was create to demonstrate aninhed serialization. It's used inside "SomeClass".
/// </summary>
[Serializable]//Using that our class is serialized
public class SomeOtherClass
{
	public string myString;
	public int myInteger;
}
}