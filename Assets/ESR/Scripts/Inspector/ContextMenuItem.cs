﻿using UnityEngine;
namespace YJack.ESR
{
/// <summary>
/// There's some additional tricks that you can use. Shouldn't be awesome if you can add some option on the right click over a variable name? In order to achieve that you can use Context Menu Item, which is a property that receive a label and an function to call.
/// </summary>
public class ContextMenuItem : MonoBehaviour
{
	[ContextMenuItem("Add", "AddMethod")] //Here we create the contextMenuItem Add in our context menu and link it to our "AddMethod". (Right click over the variable name)
	[ContextMenuItem("Subtract", "SubtractMethod")]//Here we create the contextMenuItem Subtract in our context menu and link it to our "SubtractMethod".(Right click over the variable name)
    public int someInteger;
	
	void AddMethod ()
	{
		someInteger++;
	}
	
	void SubtractMethod ()
	{
		someInteger--;
	}
}
}