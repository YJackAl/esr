﻿using UnityEngine;
using UnityEditor;
namespace yjack.ESR
{
    /// <summary>
    /// We've added a Context option from outside the used component.
    /// </summary>
    public class ContextMenuExample2
    {
	    [MenuItem("CONTEXT/ContextMenuExample/Another Extra Option")]//Here we used the Special path: CONTEXT. Unity Doc also points to Asset and Assets/Create
	    private static void AnotherExtraOption (MenuCommand menuCommand)//From Unity doc: Used to extract the context for a MenuItem. MenuCommand objects are passed to custom menu item functions defined using the MenuItem attribute.
        {
		    Debug.Log ("You can find more about this one at ContextMenuExample2.cs");
	    }
    }
}