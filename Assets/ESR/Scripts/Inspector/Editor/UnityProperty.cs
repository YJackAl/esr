﻿using UnityEngine;
using UnityEditor;

namespace yjack.ESR
{
[ExecuteInEditMode]
[DisallowMultipleComponent]
[RequireComponent(typeof(Rigidbody))]
[AddComponentMenu("My Custom Scripts/My Custom Component")]
public class UnityProperty : MonoBehaviour
{
#if UNITY_5
	[InitializeOnLoadMethod] //UnityEditor is needed.  UNITY 5 ONLY
#endif

	void HowIDidThat ()
	{
		Debug.Log("Welcome, how I was printed?");
	}
#if UNITY_5
	[RuntimeInitializeOnLoadMethod] //  UNITY 5 ONLY
#endif

	void NotAStart ()
	{
		Debug.Log("Hey, I've being called!");
	}

	void Update ()
	{
		transform.Translate(transform.position + Vector3.forward);
	}
}
}