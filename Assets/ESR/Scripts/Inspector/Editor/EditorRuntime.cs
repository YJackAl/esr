﻿using UnityEngine;
namespace yjack.ESR
{
/// <summary>
/// So you did an awesome code and just can see how it works in runtime because on editor he is just an ordinary boring object. No problem, you can make your code run on the editor.
/// </summary>
[ExecuteInEditMode]
public class EditorRuntime : MonoBehaviour
{
    public float radius = 10;
    private Renderer myRenderer;
#if UNITY_5
    [UnityEditor.InitializeOnLoadMethod] //Take a reference to be used onUpdate. UNITY 5 ONLY
#endif
    void Start ()
    {
        myRenderer = GetComponent<Renderer>();
    }
	void Update ()
    {
        myRenderer.material.color = new Color(Random.value, Random.value, Random.value);
        transform.LookAt(Vector3.zero);
	}
}
}