﻿using UnityEngine;
namespace YJack.ESR
{
/// <summary>
/// We used here native Decorator Drawer and Property Drawer. 
/// Beside the default way that a built-in serialization is done, there are some special properties that give us some additional features. Some are just decorative and are called Decorator Drawer, others, changes default serialization behavior and are called Property Drawer:
/// </summary>
public class PropertyDrawers : MonoBehaviour
{
	[Header("A Header")]//This is a Decorator Drawer. It helps to organize your inspector.
	[Tooltip("A tip when Mouse Over")]//This is a Decorator Drawer. Adds a tooltip to your exposed variable so you can complement the variable name with useful information for your user
    public bool myBool;
	[Space(20)]////This is a Decorator Drawer. It adds some space between this serialization and the next one
    [SerializeField]// This is a property drawer. It imply the serialization for the variable below even if it's private.
	private int myInteger;
	[Range(0,100)]// This is a property drawer. It describes an interface for the float variable below.
    public float myFloat;
	[HideInInspector]// This is a property drawer. It imply the non-serialization for the variable below even if it's public
    public float myOtherFloat;
	[Header("Another Header.")]//Already explained. :)
	[Multiline]// This is a property drawer. Similar to TextArea, It describes an interface for the string variable below.
    public string myString;
	[TextArea]// This is a property drawer. Similar to Multiline, It describes an interface for the string variable below.
    public string myOtherString;
}
}