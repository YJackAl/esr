﻿using UnityEngine;
namespace YJack.ESR
{
/// <summary>
/// That doesn't demand code and is all done into the Unity Editor. Please refer to the Readme for more information.
/// </summary>
public class NotAScript : MonoBehaviour
{
}
}