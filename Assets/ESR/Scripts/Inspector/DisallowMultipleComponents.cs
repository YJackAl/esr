﻿using UnityEngine;
namespace YJack.ESR
{
/// <summary>
/// We can have only one of this class into our component. Editor will block attempt to add it more then once.
/// </summary>
[DisallowMultipleComponent]
public class DisallowMultipleComponents : MonoBehaviour
{
}
}