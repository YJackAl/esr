﻿using UnityEngine;
namespace YJack.ESR
{
/// <summary>
/// Maybe your interest isn't so much in your code-part but in the component itself. You can call some methods from the component option (The little gear at the right upper corner of your component).In order to achieve that you should use the attribute Context Menu as showed below.
/// </summary>
public class ContextMenuExample : MonoBehaviour
{
	[ContextMenu("Extra Option")]//Here we add an ContextMen called "Extra Option" and link it to "ExtraOption" method below. Click over the cog on the right upper sider of your component to see this option.
	void ExtraOption ()
	{
		Debug.Log ("An extraOption was Selected");
	}
}
}