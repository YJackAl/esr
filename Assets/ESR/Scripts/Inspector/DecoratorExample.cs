﻿using UnityEngine;
namespace YJack.ESR
{
    /// <summary>
    /// This class uses a Custom DecoratorDrawer described into "CustomDecorator.cs". In that way we can see the minor impact of Custom Properties and Custom Decorator in our MonoBehaviour.
    /// </summary>
    public class DecoratorExample : MonoBehaviour
    {
        public bool beforeDecorator;
        [SeparatorAttribute("Something")]
        public bool AfterDecorator;
    }
}