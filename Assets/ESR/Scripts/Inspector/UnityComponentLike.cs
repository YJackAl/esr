﻿using UnityEngine;
using System.Collections;
namespace YJack.ESR
{
[DisallowMultipleComponent]
[RequireComponent(typeof(Rigidbody))]
[AddComponentMenu("My Custom Scripts/My Custom Component")]
public class UnityComponentLike : MonoBehaviour
{
	//Do a lot of stuff
}
}