﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace YJack.ESR
{
public class Ping : MonoBehaviour
{
}

#if UNITY_EDITOR
[CustomEditor(typeof(Ping))]
public class UserControllerEditor : Editor
{
    public override void OnInspectorGUI()
    {
#if UNITY_4_6
        EditorGUILayout.ObjectField("Script", MonoScript.FromMonoBehaviour((MonoBehaviour)target), typeof(Ping)); //Obsolete
#endif
#if UNITY_5
        EditorGUILayout.ObjectField("Script",MonoScript.FromMonoBehaviour((MonoBehaviour)target), typeof(Ping), true); //Ping this guy into Project menu.
#endif
	}
}
#endif
}