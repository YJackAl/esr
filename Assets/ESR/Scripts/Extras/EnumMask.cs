﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace YJack.ESR
{
/// <summary>
/// Everything you want to know about flags and were afraid to ask. :)
/// </summary>
public class EnumMask : MonoBehaviour
{
        public MaskedEnum maskedEnum;

    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Plus) || Input.GetKeyDown(KeyCode.KeypadPlus))
            maskedEnum |= MaskedEnum.Something;

        if (Input.GetKeyDown(KeyCode.Minus) || Input.GetKeyDown(KeyCode.KeypadMinus))
            maskedEnum &= ~MaskedEnum.Something;

        if ((maskedEnum & MaskedEnum.Something) == MaskedEnum.Something)
        {
            Debug.Log("MaskedEnum.Something is selected");
        }
    }

}

#if UNITY_EDITOR
[CustomEditor(typeof(EnumMask))]
public class EnumMaskInspector : Editor
{
    public override void OnInspectorGUI()
    {
        EnumMask enumMask = (EnumMask)target;
#if UNITY_2017_3_OR_NEWER
            enumMask.maskedEnum = (MaskedEnum)EditorGUILayout.EnumFlagsField("Options:", enumMask.maskedEnum);
#else
            enumMask.maskedEnum = (MaskedEnum)EditorGUILayout.EnumMaskField("Options:", enumMask.maskedEnum);
#endif
        EditorGUILayout.LabelField("Use \"+\" and \"-\" at runtime to turn on/off Something");
    }
}
#endif

public enum MaskedEnum
{
    Something = 1,
    SomethingElse = 2,
    OtherOption = 4,
    AnotherOption = 8,
    MoreOption = 16,
    SoMuchOption = 32,
    AlmostEndingMyOption = 64,
    AlmostThere = 128,
    Ending = 256,
    TheLastOne = 512
}
}