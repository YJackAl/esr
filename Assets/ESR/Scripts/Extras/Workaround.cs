﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace YJack.ESR
{
    public class Workaround : MonoBehaviour
    {
        [TextWithGUILayout()] //Print "Workaround using EditorGUI" into inspector. Ignore the parameter below.
        public float TextWithGUILayout;
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(Workaround))]
    public class EmptyEditorClass : Editor
    {
        //This way enables UnityEditor to use GUILayout when not allowed. We don't even need to override OnInspectorGUI for that!
    }
#endif
}