﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace YJack.ESR
{
    /// <summary>
    /// A simple Custom Property Atribute using EditorGUILayout can only be used by classes with Editor-Side code.
    /// </summary>
    public class TextWithGUILayout : PropertyAttribute
    {
        public TextWithGUILayout()
        { }
    }

#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(TextWithGUILayout))]
    public class TextWithGUILayoutDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUILayout.LabelField("Workaround using EditorGUI");
        }
    }
#endif
}